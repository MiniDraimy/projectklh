<?php

namespace App\DataFixtures;

use App\Entity\Ingredients;
use App\Entity\Recette;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void {
        $recettes = [
            ['title' => 'Salade de pâtes', 'subtitle'  => 'Entrée'],
            ['title' => 'Steak frites', 'subtitle' => 'Plat'],
            ['title' => 'Eclair au café', 'subtitle' => 'Dessert'],
            ['title' => 'Gateau au chocolat', 'subtitle' => 'Dessert'],
        ];

        foreach($recettes as $r)  {
            $recette = new Recette();
            $recette->setTitle($r['title'])
                ->setSubtitle($r['subtitle']);
            $manager->persist($recette);
        }
        $ingredients = [
            ['name' => 'Pâtes', 'recette_id'  => '1'],
            ['name' => 'Maïs', 'recette_id' => '1'],
            ['name' => 'Thon', 'recette_id' => '1'],
            ['name' => 'Tomates', 'recette_id' => '1'],
            ['name' => 'Steaks', 'recette_id' => '2'],
            ['name' => 'Pommes de terre', 'recette_id' => '2'],
            ['name' => 'Chou', 'recette_id' => '3'],
            ['name' => 'Crème patissère café', 'recette_id' => '3'],
            ['name' => 'Chocolat', 'recette_id' => '4'],
            ['name' => 'Farine', 'recette_id' => '4'],
            ['name' => 'Sucre', 'recette_id' => '4'],
            ['name' => 'Levure', 'recette_id' => '4'],
            ['name' => 'Oeufs', 'recette_id' => '4'],
        ];
        $manager->flush();

        foreach($ingredients as $i)  {
            $ingredient = new Ingredients();
            $recette = $manager->getRepository(Recette::class)->find($i['recette_id']);
            $ingredient->setName($i['name'])
                ->setRecette($recette);
            $manager->persist($ingredient);
        }

        $manager->flush();
    }
}
