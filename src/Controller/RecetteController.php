<?php

namespace App\Controller;

use DateTime;
use App\Entity\Recette;
use App\Entity\Ingredients;
use App\Repository\recetteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Annotation\Groups;

class RecetteController extends AbstractController {

    private $manager;

    public function __construct(EntityManagerInterface $manager) {
        $this->manager = $manager;
    }

    /**
     * @Route("/api/recettes", name="get_recettes", methods={"GET"})
     * @Route("/api/recettes/{id}", name="get_recette", methods={"GET"})
     * @return Response
     */
    public function getRecettes(int $id = null): Response {
        if (is_null($id)){
            $recettes = $this->manager->getRepository(Recette::class)->findAll();
        }
        else{
            $recettes = $this->manager->getRepository(Recette::class)->find($id);
        }
        $serializer = $this->container->get('serializer');
        $res = $serializer->serialize($recettes, 'json', ['groups' =>['B']], 'charset=utf8');
        return new Response($res);
    }

        /**
     * @Route("/api/recettes/{id}", name="put_recettes", methods={"PUT"})
     * @return Response
     */
    public function putRecettes(int $id, Request $request): Response {
        $data = json_decode($request->getContent(), false);
        if(is_null($data)) {return new Response('undefined recette');}
        $recette = $this->manager->getRepository(Recette::class)->find($id);
        $recette->setTitle($data->title);
        $recette->setSubtitle($data->subtitle);
        $recette->getIngredients();
        foreach($recette->getIngredients() as $i){
            $this->manager->remove($i);
        }
        $recette->removeAllIngredients();
        foreach ($data->ingredients as $i){
            $ingredient = new Ingredients();
            $ingredient->setName($i->name);
            $ingredient->setRecette($recette);
            $this->manager->persist($ingredient);
            $recette->addIngredient($ingredient);
        }
        $this->manager->persist($recette);
        $this->manager->flush();   
        $serializer = $this->container->get('serializer');
        $res = $serializer->serialize($recette, 'json', ['groups' =>['B']], 'charset=utf8');
        return new Response($res);
    }

    /**
     * @Route("/api/recettes", name="post_recettes", methods={"POST"})
     * @return Response
     */
    public function postRecettes(Request $request): Response {
        $data = json_decode($request->getContent(), false);
        if(is_null($data)) {return new Response('undefined recette');}
        $recette = new Recette();
        $recette->setTitle($data->title);
        $recette->setSubtitle($data->subtitle);
        foreach ($data->ingredients as $i){
            $ingredient = new Ingredients();
            $ingredient->setName($i->name);
            $ingredient->setRecette($recette);
            $this->manager->persist($ingredient);
            $recette->addIngredient($ingredient);
        }
        $this->manager->persist($recette);
        $this->manager->flush();   
        $serializer = $this->container->get('serializer');
        $res = $serializer->serialize($recette, 'json', ['groups' =>['B']], 'charset=utf8');
        return new Response($res);
    }

    /**
     * @Route("/api/recettes/{id}", name="del_recettes", methods={"DELETE"})
     * @return Response
     */
    public function deleteRecettes(int $id, Request $request): Response {
        $recette = $this->manager->getRepository(Recette::class)->find($id);
        if(is_null($recette)) {return new Response('undefined recette');}
        foreach($recette->getIngredients() as $i){
            $this->manager->remove($i);
        }
        $this->manager->remove($recette);
        $this->manager->flush();   
        return new Response("Recette deleted");
    }


}